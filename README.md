<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 04-30-2024 and will be end of life on 04-30-2025. The capabilities of this Pre-Built have been replaced by the [Juniper - JUNOS - IAG](https://gitlab.com/itentialopensource/pre-built-automations/juniper-junos-iag)

<!-- Update the below line with your pre-built name -->
# JunOS Software Upgrade

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Pre-Post Checks](#metrics)
* [Additional Information](#additional-information)

## Overview
This workflow is built and tested on JunOS vSRX devices to perform software upgrades according to Juniper's documentation. The workflow support Zero-Touch mode of this operation is used to minimize user interactions on happy path scenarios. This workflow can be run on either JunOS devices on-boarded on NSO or IAG (Ansible) systems. During the orchestrator discovery phase, this workflow will determine automatically how to execute commands (RPC for NSO devices).  
<!-- Write a few sentences about the pre-built and explain the use case(s) -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->

<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->
<!-- REPLACE COMMENT BELOW WITH IMAGE OF YOUR MAIN WORKFLOW -->

<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->
_Estimated Run Time_: 45 minutes

## Installation Prerequisites

Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2021.2`

## Requirements

This pre-built requires the following:

<!-- Unordered list highlighting the requirements of the pre-built -->
<!-- EXAMPLE -->
<!-- * cisco ios device -->
<!-- * Ansible or NSO (with F5 NED) * -->
* JunOS device with new firmware binary file located locally 
* Either NSO or IAG system connected to IAP

## Features

The main benefits and features of this pre-built are outlined below.

<!-- Unordered list highlighting the most exciting features of the pre-built -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->
* Allows zero touch mode of operation
* Verify the file exists on the local file system.
* Optional validate stage prior to the software add command.
* Confirms device connectivity post system reboot.
* Allows for a configuration snapshot to be taken pre and post operation. 
* Orchestrator agnostic workflow (IAG / NSO).

## How to Install

To install the pre-built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the pre-built. 
* The pre-built can be installed from within App-Admin_Essential. Simply search for the name of your desired pre-built and click the install button.

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this pre-built can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use Operations Manager in order to supply all necessary fields in the form.
Since validate and add commands may take a significant amount of time, it is recommended to adjust timeouts accordingly on IAG(Ansible) or NSO.

<!-- Explain the main entrypoint(s) for this pre-built: Automation Catalog item, Workflow, Postman, etc. -->

## Metrics

**Command Template Results**
If zero-touch is disabled, you will be presented with command template results for pre and post checks and the template results' difference for JUNOS Software Upgrade.

## Additional Information

Please use your Itential Customer Success account if you need support when using this pre-built.
