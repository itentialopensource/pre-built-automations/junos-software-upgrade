
## 0.0.7 [07-11-2024]

Add deprecation notice and metadata

See merge request itentialopensource/pre-built-automations/junos-software-upgrade!12

2024-07-11 15:36:13 +0000

---

## 0.0.6 [06-21-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/junos-software-upgrade!11

---

## 0.0.5 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/junos-software-upgrade!10

---

## 0.0.4 [12-16-2021]

* Update to 2021.2

See merge request itentialopensource/pre-built-automations/junos-software-upgrade!9

---

## 0.0.3 [07-02-2021]

* Update package.json, README.md files

See merge request itentialopensource/pre-built-automations/junos-software-upgrade!8

---

## 0.0.2 [03-05-2021]

* patch/DSUP-886

See merge request itentialopensource/pre-built-automations/junos-software-upgrade!7

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n\n
